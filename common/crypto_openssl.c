/*
 * crypto_openssl.c - Crypto wrapper (openssl version)
 */

/*
 *  Copyright (C) 2011 Gernot Tenchio
 *  Copyright (C) 2019 Christian Beier
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
 */

#include <string.h>
#include <openssl/sha.h>
#include <openssl/md5.h>
#include <openssl/dh.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include "crypto.h"

// ====== afl patch ====================
#include "/home/ubuntu/reface/reface.h"

extern signed int   afl_reface_enabled;
extern signed int   afl_len_remain;
extern unsigned char *   afl_buf;
extern signed int   afl_len;
extern signed int   afl_initialized;

#ifndef __AFL_FUZZ_TESTCASE_LEN

ssize_t       fuzz_len;
// unsigned char fuzz_buf[1024000];
unsigned char * fuzz_buf;

  #define __AFL_FUZZ_TESTCASE_LEN fuzz_len
  #define __AFL_FUZZ_TESTCASE_BUF fuzz_buf
  #define __AFL_FUZZ_INIT() void sync(void);
  #define __AFL_LOOP(x) \
    ((fuzz_len = read(0, fuzz_buf, sizeof(fuzz_buf))) > 0 ? 1 : 0)
  #define __AFL_INIT() sync()
  
  #define RAW_FUZZ

#endif


// Should insert at the file that needs the afl_fuzz_ptr
__AFL_FUZZ_INIT();

// =======================================

static unsigned char reverseByte(unsigned char b) {
   b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
   b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
   b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
   return b;
}


// ====== afl patch ===================
int patch_function(void *out, const size_t out_len, const void *in, const size_t in_len, const char* func_name)
{   
    fprintf(stderr, "%s: invoke\n", func_name);
#ifdef dump
    // dump the output into a file
    char* dumpfile = malloc(0x20);
    sprintf(dumpfile, "%s_%d", func_name, afl_reface_enabled);
    FILE *dumpfp = NULL;
    dumpfp = fopen(dumpfile, "wb+");
    fwrite(out, 1, out_len, dumpfp);
    fclose(dumpfp);
#endif

    // trigger
    if (afl_reface_enabled >= 27) {
        if (!afl_initialized) {
            afl_initialized = 1;

            // fork after the "DH" and connection
            #ifdef __AFL_HAVE_MANUAL_CONTROL
            __AFL_INIT();
            #endif

#ifndef RAW_FUZZ
            afl_buf = __AFL_FUZZ_TESTCASE_BUF;
            afl_len = __AFL_FUZZ_TESTCASE_LEN;
#else
            char* inputfile = getenv("INPUT");
            int inputfd = open(inputfile, O_RDONLY);
            afl_len = lseek(inputfd, 0, SEEK_END);
            afl_buf = (char*) mmap(NULL, afl_len, PROT_READ, MAP_PRIVATE, inputfd, 0);
            close(inputfd);
#endif
            afl_len_remain = afl_len;
            if (afl_len_remain <= 0) exit(0);
            unsigned long afl_cpy_len = afl_len_remain > out_len ? afl_len_remain : out_len;
            unsigned long afl_cpy_start = afl_len - afl_len_remain;
            
            memcpy(out, afl_buf + afl_cpy_start, afl_cpy_len);
            afl_len_remain -= afl_cpy_len;

            fprintf(stderr, "%s: %d bytes.\n", func_name, afl_cpy_len);

            return 0;
        } else {
            exit(0);
        }
    }
}
// ==========================================

int hash_md5(void *out, const void *in, const size_t in_len)
{
    int retv_orig = __hash_md5(out, in, in_len);
    return patch_function(out, 0x10, in, in_len, "hash_md5");
}
int __hash_md5(void *out, const void *in, const size_t in_len)
{
    MD5_CTX md5;
    if(!MD5_Init(&md5))
	return 0;
    if(!MD5_Update(&md5, in, in_len))
	return 0;
    if(!MD5_Final(out, &md5))
	return 0;
    return 1;
}


int hash_sha1(void *out, const void *in, const size_t in_len)
{
    int retv_orig = __hash_sha1(out, in, in_len);
    return patch_function(out, 20, in, in_len, "hash_sha1");
}
int __hash_sha1(void *out, const void *in, const size_t in_len)
{
    SHA_CTX sha1;
    if(!SHA1_Init(&sha1))
	return 0;
    if(!SHA1_Update(&sha1, in, in_len))
	return 0;
    if(!SHA1_Final(out, &sha1))
	return 0;
    return 1;
}

void random_bytes(void *out, size_t len)
{
    RAND_bytes(out, len);
}


int encrypt_rfbdes(void *out, int *out_len, const unsigned char key[8], const void *in, const size_t in_len)
{
    int retv_orig = __encrypt_rfbdes(out, out_len, key, in, in_len);
    return patch_function(out, *out_len, in, in_len, "encrypt_rfbdes");
}
int __encrypt_rfbdes(void *out, int *out_len, const unsigned char key[8], const void *in, const size_t in_len)
{
    int result = 0;
    EVP_CIPHER_CTX *des;
    unsigned char mungedkey[8];
    int i;

    for (i = 0; i < 8; i++)
      mungedkey[i] = reverseByte(key[i]);

    if(!(des = EVP_CIPHER_CTX_new()))
	goto out;
    if(!EVP_EncryptInit_ex(des, EVP_des_ecb(), NULL, mungedkey, NULL))
	goto out;
    if(!EVP_EncryptUpdate(des, out, out_len, in, in_len))
	goto out;

    result = 1;

 out:
    EVP_CIPHER_CTX_free(des);
    return result;
}


int decrypt_rfbdes(void *out, int *out_len, const unsigned char key[8], const void *in, const size_t in_len)
{
    int retv_orig = __decrypt_rfbdes(out, out_len, key, in, in_len);
    return patch_function(out, *out_len, in, in_len, "decrypt_rfbdes");
}
int __decrypt_rfbdes(void *out, int *out_len, const unsigned char key[8], const void *in, const size_t in_len)
{
    int result = 0;
    EVP_CIPHER_CTX *des;
    unsigned char mungedkey[8];
    int i;

    for (i = 0; i < 8; i++)
      mungedkey[i] = reverseByte(key[i]);

    if(!(des = EVP_CIPHER_CTX_new()))
	goto out;
    if(!EVP_DecryptInit_ex(des, EVP_des_ecb(), NULL, mungedkey, NULL))
	goto out;
    if(!EVP_DecryptUpdate(des, out, out_len, in, in_len))
	goto out;

    result = 1;

 out:
    EVP_CIPHER_CTX_free(des);
    return result;
}

int encrypt_aes128ecb(void *out, int *out_len, const unsigned char key[16], const void *in, const size_t in_len)
{
    int retv_orig = __encrypt_aes128ecb(out, out_len, key, in, in_len);
    return patch_function(out, *out_len, in, in_len, "encrypt_aes128ecb");
}
int __encrypt_aes128ecb(void *out, int *out_len, const unsigned char key[16], const void *in, const size_t in_len)
{
    int result = 0;
    EVP_CIPHER_CTX *aes;

    if(!(aes = EVP_CIPHER_CTX_new()))
	goto out;
    EVP_CIPHER_CTX_set_padding(aes, 0);
    if(!EVP_EncryptInit_ex(aes, EVP_aes_128_ecb(), NULL, key, NULL))
	goto out;
    if(!EVP_EncryptUpdate(aes, out, out_len, in, in_len))
	goto out;

    result = 1;

 out:
    EVP_CIPHER_CTX_free(aes);
    return result;
}

int dh_generate_keypair(uint8_t *priv_out, uint8_t *pub_out, const uint8_t *gen, const size_t gen_len, const uint8_t *prime, const size_t keylen)
{
    int result = 0;
    DH *dh;
#if OPENSSL_VERSION_NUMBER >= 0x10100000L
    const BIGNUM *pub_key = NULL;
    const BIGNUM *priv_key = NULL;
#endif

    if(!(dh = DH_new()))
	goto out;
#if OPENSSL_VERSION_NUMBER < 0x10100000L || defined LIBRESSL_VERSION_NUMBER
    dh->p = BN_bin2bn(prime, keylen, NULL);
    dh->g = BN_bin2bn(gen, gen_len, NULL);
#else
    if(!DH_set0_pqg(dh, BN_bin2bn(prime, keylen, NULL), NULL, BN_bin2bn(gen, gen_len, NULL)))
	goto out;
#endif
    if(!DH_generate_key(dh))
	goto out;
#if OPENSSL_VERSION_NUMBER < 0x10100000L || defined LIBRESSL_VERSION_NUMBER
    if(BN_bn2bin(dh->priv_key, priv_out) == 0)
	goto out;
    if(BN_bn2bin(dh->pub_key, pub_out) == 0)
	goto out;
#else
    DH_get0_key(dh, &pub_key, &priv_key);
    if(BN_bn2binpad(priv_key, priv_out, keylen) == -1)
	goto out;
    if(BN_bn2binpad(pub_key, pub_out, keylen) == -1)
	goto out;
#endif

    result = 1;

 out:
    DH_free(dh);
    return result;
}

int dh_compute_shared_key(uint8_t *shared_out, const uint8_t *priv, const uint8_t *pub, const uint8_t *prime, const size_t keylen)
{
    int result = 0;
    DH *dh;

    if(!(dh = DH_new()))
	goto out;
#if OPENSSL_VERSION_NUMBER < 0x10100000L || defined LIBRESSL_VERSION_NUMBER
    dh->p = BN_bin2bn(prime, keylen, NULL);
    dh->priv_key = BN_bin2bn(priv, keylen, NULL);
#else
    if(!DH_set0_pqg(dh, BN_bin2bn(prime, keylen, NULL), NULL, BN_new()))
	goto out;
    if(!DH_set0_key(dh, NULL, BN_bin2bn(priv, keylen, NULL)))
	goto out;
#endif
    if(DH_compute_key(shared_out, BN_bin2bn(pub, keylen, NULL), dh) == -1)
	goto out;

    result = 1;

 out:
    DH_free(dh);
    return result;
}
